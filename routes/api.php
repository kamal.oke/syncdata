<?php

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\EmployeeArgentaController;
use App\Http\Controllers\EmployeeCsController;
use App\Http\Controllers\ContohController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('register', [AuthController::class, 'register']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::get('user-profile', [AuthController::class, 'userProfile']);
});



Route::group([
    'middleware' => 'jwt.verify'
], function () {

    /* Cs */
    Route::get('employee/cs', [EmployeeCsController::class,'index']);
    Route::get('employee/createdate/cs/{tgl}', [EmployeeCsController::class,'createdate']);
    Route::get('employee/cs/search', [EmployeeCsController::class,'search']);
    
    /* Argenta */
    Route::get('employee/argenta', [EmployeeArgentaController::class,'index']);
    Route::get('employee/argenta/createdate/{tgl}', [EmployeeArgentaController::class,'createdate']);
    Route::get('employee/argenta/search', [EmployeeArgentaController::class,'search']);

});

/* Test Modul */
Route::get('/test',[TestController::class,'index']);
Route::get('/contoh',[ContohController::class,'index']);


