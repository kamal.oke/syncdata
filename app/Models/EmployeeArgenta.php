<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeArgenta extends Model
{
    use HasFactory;
    protected $connection = 'mysql_tiga';
    public $table = 'sys_vtrpt_employee_sp';
    protected $guarded = [];
}
