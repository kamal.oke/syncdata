<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeCs extends Model
{
    use HasFactory;
    protected $connection = 'mysql_dua';
    public $table = 'sys_vtrpt_employee_sp';
    protected $guarded = [];
}
