<?php

namespace App\Http\Controllers;

use Spatie\QueryBuilder\QueryBuilder;
use App\Models\EmployeeCs;
use App\Http\Helper\ResponseBuilder;
use Symfony\Component\HttpFoundation\Response;


class EmployeeCsController extends Controller
{
    public function index()
    {

        $employee = EmployeeCs::all();
        return response()->json([
            'pesan' => 'Data berhasil di ambil',
            'status' => 'sukses',
            'data' => $employee
        ]);
    }

    public function createdate($tgl)
    {
        $employee = EmployeeCs::where('createdate','=', $tgl)->get();
        $rows = $employee->count();
        return response()->json([
            'pesan' => 'Data berhasil di ambil',
            'status' => 'sukses',
            'jumlah_data'=> $rows,
            'data' => $employee
        ]);
    }

    public function search() {
        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;
        $data = QueryBuilder::for(EmployeeCs::class)
        ->allowedFilters(['nip','pin','nama_lengkap','divisi','departemen','jabatan','area_kerja','tgl_mulai_kerja','status_pegawai','status_aktif','jenis_kelamin','email_kantor','createdate'])
        ->get();

        if (empty($data)){
            $message  = "Data kosong";
            return ResponseBuilder::result('False', $message, '[]', '404');
        }

        return ResponseBuilder::result($status, $message, $data, $response_code);

    }
}
